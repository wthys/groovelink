package groovelink;

import be.ac.ua.ansymo.cheopsj.model.changes.Subject;
import be.ac.ua.ansymo.cheopsj.model.famix.FamixEntity;

public class GLSubject {
	public static String getName(Subject subject) {
		if (subject instanceof FamixEntity) {
			return ((FamixEntity) subject).getName(); 
		}
		return null;
	}
	
	public static String getFamixType(Subject subject) {
		return subject.getFamixType();
	}

}
