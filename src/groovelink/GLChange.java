package groovelink;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import be.ac.ua.ansymo.cheopsj.model.changes.AtomicChange;
import be.ac.ua.ansymo.cheopsj.model.changes.Change;
import be.ac.ua.ansymo.cheopsj.model.changes.Subject;

public class GLChange {
	private static Map<Integer, Change> changeRegistry = new HashMap<Integer, Change>();
	
	public static String getFamixType(Change change) {
		return change.getFamixType();
	}
	
	public static String getChangeType(Change change) {
		return change.getChangeType();
	}
	
	public static Subject getSubject(Change change) {
		if (change instanceof AtomicChange) {
			return ((AtomicChange) change).getChangeSubject();
		}
		return null;
	}
	
	public static long getTimestamp(Change change) {
		return change.getTimeStamp().getTime();
	}
	
	public static java.util.Collection<Integer> getDependencies(Change change) {
		java.util.Collection<Integer> serials = new LinkedList<Integer>();
		for (Change dep : change.getStructuralDependencies()) {
			for (Entry<Integer, Change> entry: changeRegistry.entrySet()) {
				if (entry.getValue().equals(dep)) {
					serials.add(entry.getKey());
				}
			}
		}
		return serials;
	}

	public static void register(int serial, Change change) {	
		if (change == null || changeRegistry.containsValue(change)) {
			return;
		}
		changeRegistry.put(serial, change);
	}
	
	public static Change retrieve(int serial) {
		return changeRegistry.get(serial);
	}
}
