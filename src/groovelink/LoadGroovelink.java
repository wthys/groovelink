package groovelink;
import org.eclipse.ui.IStartup;


public class LoadGroovelink implements IStartup {

    @Override
    public void earlyStartup() {
        Groovelink.getInstance().startPlugin();
    }

}
