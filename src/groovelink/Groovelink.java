package groovelink;

import static groove.transform.RuleEvent.Reuse.NONE;
import groove.explore.result.Acceptor;
import groove.explore.strategy.Consultant;
import groove.explore.strategy.ConsultantStrategy;
import groove.explore.strategy.Strategy;
import groove.grammar.Grammar;
import groove.grammar.model.FormatException;
import groove.grammar.model.GrammarModel;
import groove.io.store.DefaultFileSystemStore;
import groove.io.store.SystemStore;
import groove.lts.GTS;
import groove.lts.GTSAdapter;
import groove.lts.GraphState;
import groove.lts.GraphState.Flag;
import groove.lts.MatchResult;
import groove.lts.RuleTransition;
import groove.transform.Record;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.logging.Logger;

import org.eclipse.jface.dialogs.MessageDialog;

import be.ac.ua.ansymo.cheopsj.model.ModelManagerEvent;
import be.ac.ua.ansymo.cheopsj.model.ModelManagerListener;
import be.ac.ua.ansymo.cheopsj.model.ModelManagerListeners;
import be.ac.ua.ansymo.cheopsj.model.changes.Change;
import be.ac.ua.ansymo.cheopsj.model.changes.IChange;

public class Groovelink extends Consultant implements ModelManagerListener {

    private static Groovelink instance;

    private Strategy strategy;

    private GTS gts;
    private GraphState startState;
    private GraphState nextState;
    private Stack<GraphState> pool = new Stack<GraphState>();
    private Acceptor acceptor;
    private ExploreListener exploreListener = new ExploreListener();
    private boolean paused = false;

    private Queue<Change> changeQueue = new LinkedList<Change>();
    private static Logger LOG = Logger.getGlobal();

    private Groovelink() {
        System.out.println("Groovelink instance created!");
        // Add our protocol handlers for file loading.
        /*
         * System.out.println("Registering protocol handler"); final String key
         * = "java.protocol.handler.pkgs"; String newValue =
         * "groovelink.protocols"; if (System.getProperty(key) != null) { final
         * String previousValue = System.getProperty(key); newValue += "|" +
         * previousValue; } System.setProperty(key, newValue);
         */

        // Set up ChEOPSJ

        // Set up for Groove
        System.out.println("Setting up Groove grammar");
        strategy = new ConsultantStrategy(this);
        // TODO load grammar stuff thingy
        try {
            URL gramUrl;
            gramUrl = new URL("platform:/plugin/be.wimthys.dev.groovelink/detector.zip");
            //gramUrl = ClassLoader.getSystemResource("detector.zip");
            System.out.println(gramUrl);
            //SystemStore ss = new DefaultFileSystemStore(gramUrl);
            //GrammarModel gm = new GrammarModel(ss);
            GrammarModel gm = GrammarModel.newInstance(gramUrl);
            Grammar grammar = gm.toGrammar();
            GTS gts = new GTS(grammar);

            strategy.setGTS(gts);
            strategy.play(this);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Groovelink getInstance() {
        if (instance == null) {
            instance = new Groovelink();
        }
        return instance;
    }

    /**************************************************************************
     * methods for Groove
     **************************************************************************/

    /**
     * Gets a change from the change queue.
     * 
     * @param serial
     *            The serial to be used. Every invocation with this serial will
     *            always result in the same change.
     * @return The change associated with the serial.
     */
    public static Object getChange(int serial) {
        Change change = instance.changeQueue.poll();
        if (change != null) {
            GLChange.register(serial, change);
        }
        return change;
    }

    public static boolean detectRefactoring(String type, int from_serial,
            int to_serial) {

        Change from = GLChange.retrieve(from_serial);
        Change to = GLChange.retrieve(to_serial);

        if (from == null || to == null) {
            return false;
        }
        if (type.equals("Move:method")) {
            MessageDialog error = new MessageDialog(null, "Refactoring found!",
                    null, "You moved a method, congratulations!",
                    MessageDialog.ERROR, new String[] { "Great, thanks" }, 0);
            error.open();
        } else if (type.equals("Rename:method")) {
            MessageDialog error = new MessageDialog(null, "Refactoring found!",
                    null, "You renamed a method, congratulations!",
                    MessageDialog.ERROR, new String[] { "Great, thanks" }, 0);
            error.open();
        }

        return true;
    }

    public boolean hasChanges() {
        return !instance.changeQueue.isEmpty();
    }

    /**************************************************************************
     * ModelManagerListener methods
     **************************************************************************/

    @Override
    public void changesAdded(ModelManagerEvent event) {
        System.out.println("Adding changes to model");
        for (IChange change : event.getNewChanges()) {
            changeQueue.add((Change) change);
        }

        strategy.play(this);
    }

    @Override
    public void refresh() {
        // say whut?
    }

    /**************************************************************************
     * Consultant methods
     **************************************************************************/

    @Override
    public void prepare(GTS gts, GraphState state, Acceptor acceptor) {
        if (this.paused) {
            this.paused = false;
            return;
        }

        Record record = gts.getRecord();
        record.setCollapse(false);
        record.setCopyGraphs(false);
        record.setReuseEvents(NONE);
        this.gts = gts;
        this.startState = (state == null ? gts.startState() : state);
        this.acceptor = acceptor;
        if (acceptor != null) {
            gts.addLTSListener(acceptor);
        }
        gts.addLTSListener(this.exploreListener);
        this.nextState = this.startState;
    }

    @Override
    public GraphState doNext() {
        GraphState next = this.nextState;
        List<MatchResult> matches = next.getMatches();
        if (hasChanges()) {
            for (MatchResult match : matches) {
                String ruleName = match.getRule().getTransitionLabel();
                if (ruleName.startsWith("insertChange")) {
                    RuleTransition rt = next.applyMatch(match);
                    this.nextState = rt.target();
                    return next;
                }
            }
        } else {
            List<MatchResult> filtered = new LinkedList<MatchResult>();
            for (MatchResult match : matches) {
                if (!(match.getRule().getTransitionLabel()
                        .startsWith("insertChange"))) {
                    filtered.add(match);
                }
            }
            if (filtered.isEmpty()) {
                pauseExploration();
                return null;
            } else {
                MatchResult match = filtered.get(0);
                if (!next.isClosed()) {
                    this.pool.push(next);
                }
                next.applyMatch(match);
            }
        }
        if (this.pool.isEmpty()) {
            this.nextState = null;
        } else {
            this.nextState = this.pool.pop();
        }
        return next;
    }

    private void pauseExploration() {
        this.paused = true;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public void finish() {
        if (this.acceptor != null) {
            this.gts.removeLTSListener(this.acceptor);
        }
    }

    @Override
    public boolean halt() {
        return false;
    }

    private class ExploreListener extends GTSAdapter {
        @Override
        public void addUpdate(GTS gts, GraphState state) {
            // empty the pool if the new state is not transient
            // as then no more backtracking is going to be needed
            if (!state.isTransient()) {
                for (GraphState s : pool) {
                    s.setClosed(false);
                }
                pool.clear();
            }
            // only add non-transient states if they are unknown
            if (state.isTransient() || !state.hasFlag(Flag.KNOWN)) {
                pool.push(state);
            }
        }
    }

    public void startPlugin() {
        System.out.println("Setting up ChEOPSJ link");
        ModelManagerListeners.getInstance().addModelManagerListener(this);
    }

    public void stopPlugin() {
        ModelManagerListeners.getInstance().removeModelManagerListener(this);
    }
}
