package groovelink;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;


public class Activator extends AbstractUIPlugin {

    public Activator() {
        
    }
    
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        Groovelink.getInstance().startPlugin();
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        Groovelink.getInstance().stopPlugin();
        super.stop(context);
    }

}
