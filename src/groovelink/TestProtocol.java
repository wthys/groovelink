package groovelink;

import groove.explore.strategy.Strategy;
import groove.grammar.Grammar;
import groove.grammar.model.FormatException;
import groove.grammar.model.GrammarModel;
import groove.lts.GTS;
import groovelink.protocols.classpath.Handler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class TestProtocol {

    public static void main(String[] args) {
        System.out.println("Registering protocol handler");
        final String key = "java.protocol.handler.pkgs";
        String newValue = "groovelink.protocols";
        if (System.getProperty(key) != null) {
            final String previousValue = System.getProperty(key);
            newValue += "|" + previousValue;
        }
        System.setProperty(key, newValue);
        try {
            URL url = new URL(null, "classpath:detector.gps",
                    new Handler(ClassLoader.getSystemClassLoader()));
            
            URL bla = ClassLoader.getSystemClassLoader().getResource("detector.gps");
            
            System.out.println(bla);

            System.out.println(url.getPath());
            
            //File f = new File(url.toURI());

            System.out.println("loading url");
            GrammarModel gm = GrammarModel.newInstance(bla);
            System.out.println("to grammar");
            Grammar gram = gm.toGrammar();
            System.out.println("creating gts");
            GTS gts = new GTS(gram);
            System.out.println("creating strategy");
            Strategy strat = new groove.explore.strategy.BFSStrategy();
            System.out.println("setting gts");
            strat.setGTS(gts);
            System.out.println("playing strategy");
            strat.play();
            //GrammarModel gm = GrammarModel
            //        .newInstance("classpath:detector.gps");
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
