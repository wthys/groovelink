package groovelink.protocols.classpath;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Handler to load stuff using the {@link ClassLoader#getResource(String)}. 
 * 
 * Taken from <a href="http://stackoverflow.com/questions/86150">Stack Overflow</a>.
 * 
 * More info on <a href="http://docs.oracle.com/javase/6/docs/api/java/net/URL.html#URL(java.lang.String, java.lang.String, int, java.lang.String)">oracle.com</a>.
 * @author Wim Thys
 *
 */
public class Handler extends URLStreamHandler {
    
    private final ClassLoader loader;
    
    public Handler() {
        this.loader = getClass().getClassLoader();
    }
    
    public Handler(ClassLoader loader) {
        this.loader = loader;
    }

	@Override
	protected URLConnection openConnection(URL u) throws IOException {
		final URL resourceUrl = loader.getResource(u.getPath());
		System.out.println("Loading `" + resourceUrl + "'");
		return resourceUrl.openConnection();
	}

}
